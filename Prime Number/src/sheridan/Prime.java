package sheridan;

public class Prime {
	
	
	public static boolean isPrime( int number ) {
		for ( int i = 2 ; i <= number /2 ; i ++) {
			if ( number % i == 0  ) {
				return false;
			}
		}
		return true;
 	}

	public static void main(String[] args) {
		

		System.out.println( "Number 29 is prime? " + Prime.isPrime( 29 ) );
		System.out.println( "Number 29 is prime? " + Prime.isPrime( 30 ) );		
		System.out.println( "Number 33 is prime? " + Prime.isPrime( 33 ) );
		System.out.println( "Number 33 is prime? " + Prime.isPrime( 34 ) );	
		System.out.println( "Number 33 is prime? " + Prime.isPrime( 35 ) );	
		
		//added line in personal branch
		System.out.println( "Number 33 is prime? " + Prime.isPrime( 76 ) );	
			
		//added line in personal branch from Mansi
		System.out.println( "Number 7 is prime? " + Prime.isPrime( 7 ) );
		
		//second added line in personal branch
		System.out.println( "Number 6 is prime? " + Prime.isPrime( 6 ) );	
		
		//added line in personal branch
		System.out.println( "Number 76 is prime? " + Prime.isPrime( 76 ) );	
		
		//added line in personal branch from Mansi
		System.out.println( "Number 7 is prime? " + Prime.isPrime( 7 ) );

		//added line in personal branch from Mansi
		System.out.println( "Number 10 is prime? " + Prime.isPrime( 10 ) );
		
	
		
	}

}
